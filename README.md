# Around Advice

The purpose of this task is to understand and implement the Spring AOP concept of Around Advice.

Duration: *20 minutes*

## Description

You are provided with a `TrafficService` class that simulates a delay and returns a string specifying the length of the delay.

Your task is to use Spring AOP to create `Around` advice that logs additional information about the `doIt()` method of the `TrafficService` class.

## Requirements

1. Enable support for handling components marked with AspectJ's `@Aspect` annotation. *Hint:* Add the appropriate annotation for the `AppConfig` class.

2. Implement the aspect class `LoggingAspect`: 
   - This aspect should have `Around` advice that matches the `doIt()` method in the `TrafficService` class.
   - This `Around` advice should do the following:
      - Log the method signature
      - Log the arguments that the method accepts
      - Calculate the time before and after invocating the method to calculate how long it takes the method to execute
      - Log how long it took the method to execute
      - Log the result returned by the method 

3. When the `TrafficService.doIt` method executes, the logs should include: 
   - The method signature
   - The arguments passed to the method
   - How long it took the method to execute
   - The string returned by the method 

## Examples

An example of the `main` method is given below.

```java
public static void main(String[] args) {
    try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
        TrafficService service = context.getBean("trafficService", TrafficService.class);
        service.doIt(MIN_TIMEOUT, MAX_TIMEOUT);
    }
}
```

A sample output with aspect handling **disabled** is shown below.

```txt
The service has started... Finished.
```

A sample output with aspect handling **enabled** is shown below.

```txt
>>> Signature: TrafficService.doIt(..)
>>> Arguments: [1000, 3000]
The service has started... Finished.
>>> Duration: 2.277 seconds
>>> Returned: Service delay: 2.272 seconds
```
