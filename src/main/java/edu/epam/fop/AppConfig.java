package edu.epam.fop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("edu.epam.fop")
@EnableAspectJAutoProxy
public class AppConfig {
}
