package edu.epam.fop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Around("execution(* edu.epam.fop.service.*.*(..))")
    public Object aroundGetFortune(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        // Log method's signature
        String methodName = proceedingJoinPoint.getSignature().toShortString();
        System.out.println(">>> Signature: " + methodName);

        // Log method's arguments before execution
        Object[] args = proceedingJoinPoint.getArgs();
        System.out.println(">>> Arguments: " + java.util.Arrays.toString(args));

        long startTime = System.currentTimeMillis();

        // Invoke the actual method
        Object result = proceedingJoinPoint.proceed();

        long endTime = System.currentTimeMillis();

        // Calculate duration
        double durationInSeconds = (endTime - startTime) / 1000.0;

        // Log time taken by the method's execution
        System.out.println(">>> Duration: " + durationInSeconds + " seconds");

        // Log returned result
        System.out.println(">>> Returned: " + result);

        return result;
    }
}
