package edu.epam.fop.service;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class TrafficService {
    public String doIt(int minTimeout, int maxTimeout) {
        System.out.print("The service has started... ");
        // Simulate a delay
        int timeout = ThreadLocalRandom.current().nextInt(minTimeout, maxTimeout + 1);
        try {
            TimeUnit.MILLISECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            System.out.println("Interrupted.");
        }
        System.out.println("Finished.");
        return "Service delay: " + timeout / 1000.0 + " seconds";
    }
}
