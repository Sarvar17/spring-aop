package edu.epam.fop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import edu.epam.fop.service.TrafficService;

public class Main {

    public static final int MIN_TIMEOUT = 1000;
    public static final int MAX_TIMEOUT = 3000;

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
            TrafficService service = context.getBean("trafficService", TrafficService.class);
            service.doIt(MIN_TIMEOUT, MAX_TIMEOUT);
        }
    }
}
